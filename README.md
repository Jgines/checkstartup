# checkstartup

[![Build Status](https://travis-ci.com/fmrico/checkstartup.svg?token=audmcQXBNcy1dz68gcay&branch=master)](https://travis-ci.com/fmrico/checkstartup)

- Author: Francisco Martín Rico fmrico@gmail.com (Intelligent Robotics Core S.L www.inrobots.es)
- License: BSD

## Requirements

- Ubuntu 16.04 LTS or Ubuntu 18.04 LTS
- ROS Kinetic or Melodic

## Usage

- After build, run `checkstartup_info` to get the mac address of all you interfaces, and its hash:

```
paco@pegaso:~/checkstartup_ws$ rosrun checkstartup checkstartup_info
[ INFO] [1550441821.229180098]: mac: [14:c9:13:9e:c0:d3] -> [6978466449799354582]
[ INFO] [1550441821.229805388]: mac: [e0:41:36:04:5c:58] -> [8558643343958270556]
[ INFO] [1550441821.229834397]: mac: [e0:41:36:04:5c:58] -> [8558643343958270556]
[ INFO] [1550441821.229852891]: mac: [3c:8d:20:62:3c:ae] -> [12023832372382748038]
[ INFO] [1550441821.229867546]: mac: [14:c9:13:9e:c0:d3] -> [6978466449799354582]
```

- Select one of the hash number and set `SECRET` in `include\checkstartup\checkstartup.h` with it:

```
private:

  static const size_t SECRET = 6978466449799354582;
};

```
- Make any class inherit from CheckStartup class in order to check the mac:

```
#include <ros/ros.h>

#include "checkstartup/checkstartup.h"

class A: public checkstartup::CheckStartup
{
};

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "test");
  ros::NodeHandle n;

  A a;

  ros::spin();
  return 1;
}

```

- If mac hash does not correspond to any mac in the host/robot, you will get a

```
terminate called after throwing an instance of 'checkstartup::CheckStartupException'
  what():  CheckStartup exception happened. Contact with www.inrobots.es
Aborted
```
